
import styled, {css} from 'styled-components';
import {pointer} from '~/styles';

export const PrimaryButton = styled.button`
  ${pointer()}
  border: none;
  border-radius: 5px;
  font-size: 12px;
  font-weight: 700;
  height: 40px;
  line-height: 10px;
  min-width: 150px;
  opacity: 1;
  padding: 0 15px;
  text-transform: uppercase;
  transition: ease .3s;

  ${({theme}) => css`
    background-color: ${theme.colors.primary.main};
    color: ${theme.colors.primary.contrast};

    &:hover {
      background-color: ${theme.colors.primary.light};
      color: ${theme.colors.primary.contrast};
    }
  `}

  ${({disabled, theme}) => disabled && css`
    background-color: ${theme.colors.primary.light};
  `}
`;

export const SecondaryButton = styled(PrimaryButton)`
  ${({theme}) => css`
    background-color: ${theme.colors.secondary.main};
    color: ${theme.colors.secondary.contrast};

    &:hover {
      background-color: ${theme.colors.secondary.light};
      color: ${theme.colors.secondary.contrast};
    }
  `}

  ${({disabled, theme}) => disabled && css`
    background-color: ${theme.colors.secondary.light};
  `}
`;
