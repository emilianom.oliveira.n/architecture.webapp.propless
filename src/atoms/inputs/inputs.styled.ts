
import styled, {css} from 'styled-components';

export const BaseInput = styled.input`
  border: 1px solid lightgray;
  border-radius: 5px;
  height: 45px;
  padding: 0 10px;
  width: 250px;

  ${({theme}) => css`
    background-color: ${theme.colors.palette.white};
    color: ${theme.colors.text};
  `}
`;
