
import clone from 'clone';

export const deepClone = (obj: any) => clone(obj);
