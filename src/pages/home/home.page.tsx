
import {useMemo} from 'react';
import {useHookstate} from '@hookstate/core';

import {ClientInfo} from '~/components';
import {UserDataStore} from '~/stores';
import Button from './button/button';
import {
 Label, Title, Wrapper,
} from './home.page.styles';

const HomePage = () => {

  const {ip, location: {city, state}} = useHookstate(UserDataStore.state);

  const mainLabel = useMemo<string>(() => {
    let label: string;
    if(ip.get()) {
      label = `Your IP is ${ip.get()} and you're `;
      label += `at ${city.get()} - ${state.get()}.`;
    } else {
      label = 'Find out your IP clicking down bellow.';
    }

    return label;
  }, [ ip, city, state ]);

  return (
    <Wrapper>
      <Title>
        IP Finder!
      </Title>
      <Label>
        {mainLabel}
      </Label>
      <Button />
      <ClientInfo />
    </Wrapper>
  );
};

// export default observer(HomePage);

export default HomePage;
