
import styled from 'styled-components';
import {PageTitle, SecondaryButton} from '~/atoms';
import {Input} from '~/components';

export const Wrapper = styled.div`
  align-self: center;
  display: flex;
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

export const Title = styled(PageTitle)`
  align-self: center;
`;

export const EmailInput = styled(Input).attrs({
  id: 'email',
  label: 'Email',
  name: 'email',
  placeholder: 'Email here...',
})`
  margin-bottom: 20px;
`;

export const NameInput = styled(Input).attrs({
  id: 'first-name',
  label: 'Name',
  name: 'firstName',
  placeholder: 'First name here...',
})`
  margin-bottom: 20px;
`;

export const LastNameInput = styled(Input).attrs({
  id: 'last-name',
  label: 'Last',
  name: 'lastName',
  placeholder: 'Last name here...',
})`
  margin-bottom: 20px;
`;

export const PasswordInput = styled(Input).attrs({
  id: 'password',
  label: 'Password',
  name: 'password',
  placeholder: 'Password here...',
  type: 'password',
})`
  margin-bottom: 50px;
`;

export const SubmitButton = styled(SecondaryButton).attrs({
  type: 'submit',
})`
  align-self: center;
`;
