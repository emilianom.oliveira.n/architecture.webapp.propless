
import {createGlobalStyle} from 'styled-components';
import fonts from './fonts/fonts.global.theme';

export default createGlobalStyle`
  ${fonts}
  
  *, *:before, *:after {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
  }

  body {
    overflow: auto;
    font-family: sans-serif;
  }

  h1, h2, h3, h4, h5, h6 {
    font-family: Barlow, sans-serif;
  }

  label, p, input {
    font-family: "Open Sans", sans-serif;
  }

  button, input {
    &:active, &:focus, &:focus-within, &:hover, &:visited {
      outline: none;
    }
  }
  
  img {
    display: block;
  }
`;
