
import {fontFace} from 'polished';
import {FlattenSimpleInterpolation, css} from 'styled-components';

const {PUBLIC_URL} = process.env;

const setupBuilder = ({
  family,
  fileFormats,
  basePath,
  style = 'regular',
}: {
  family: string;
  fileFormats: Array<string>;
  basePath: string;
  style?: 'regular' | 'italic',
}) => {

  const builder = ({
    weight,
    fileName,
    fileFormats: builderFileFormats,
  }: {
    weight: string;
    fileName: string;
    fileFormats?: Array<string>;
  }) => fontFace({
    fileFormats: builderFileFormats || fileFormats,
    fontFamily: family,
    fontFilePath: `${PUBLIC_URL}/${basePath}/${fileName}`,
    fontStyle: style,
    fontWeight: weight,
  });

  return builder;
};

const barlow = (): FlattenSimpleInterpolation => {
  const baseParams = {
    basePath: 'fonts/barlow',
    family: 'Barlow',
    fileFormats: [ 'ttf' ],
  };
  const build = setupBuilder({...baseParams});
  const buildItalic = setupBuilder({...baseParams});

 return css`
    /* Regular */
    ${build({fileName: 'Barlow-Bold', weight: 'bold'})};
    ${build({fileName: 'Barlow-Regular', weight: 'normal'})};
    ${build({fileName: 'Barlow-Black', weight: '900'})};
    ${build({fileName: 'Barlow-ExtraBold', weight: '800'})};
    ${build({fileName: 'Barlow-Bold', weight: '700'})};
    ${build({fileName: 'Barlow-SemiBold', weight: '600'})};
    ${build({fileName: 'Barlow-Medium', weight: '500'})};
    ${build({fileName: 'Barlow-Regular', weight: '400'})};
    ${build({fileName: 'Barlow-Light', weight: '300'})};
    ${build({fileName: 'Barlow-ExtraLight', weight: '200'})};
    ${build({fileName: 'Barlow-Thin', weight: '100'})};

    /* Italic */
    ${buildItalic({fileName: 'Barlow-BoldItalic', weight: 'bold'})};
    ${buildItalic({fileName: 'Barlow-Italic', weight: 'normal'})};
    ${buildItalic({fileName: 'Barlow-BlackItalic', weight: '900'})};
    ${buildItalic({fileName: 'Barlow-ExtraBoldItalic', weight: '800'})};
    ${buildItalic({fileName: 'Barlow-BoldItalic', weight: '700'})};
    ${buildItalic({fileName: 'Barlow-SemiBoldItalic', weight: '600'})};
    ${buildItalic({fileName: 'Barlow-MediumItalic', weight: '500'})};
    ${buildItalic({fileName: 'Barlow-RegularItalic', weight: '400'})};
    ${buildItalic({fileName: 'Barlow-LightItalic', weight: '300'})};
    ${buildItalic({fileName: 'Barlow-ExtraLightItalic', weight: '200'})};
    ${buildItalic({fileName: 'Barlow-ThinItalic', weight: '100'})};
  `;
};

const openSans = (): FlattenSimpleInterpolation => {

  const baseParams = {
    basePath: 'fonts/open-sans',
    family: 'Open Sans',
    fileFormats: [ 'ttf' ],
  };

  const build = setupBuilder({...baseParams});
  const buildItalic = setupBuilder({...baseParams, style: 'italic'});

  return css`
    /* Regular */
    ${build({fileName: 'OpenSans-Bold', weight: 'bold'})};
    ${build({fileName: 'OpenSans-Regular', weight: 'normal'})};
    ${build({fileName: 'OpenSans-ExtraBold', weight: '800'})};
    ${build({fileName: 'OpenSans-Bold', weight: '700'})};
    ${build({fileName: 'OpenSans-SemiBold', weight: '600'})};
    ${build({fileName: 'OpenSans-Regular', weight: '400'})};

    /* Italic */
    ${buildItalic({fileName: 'OpenSans-BoldItalic', weight: 'bold'})};
    ${buildItalic({fileName: 'OpenSans-Italic', weight: 'normal'})};
    ${buildItalic({fileName: 'OpenSans-ExtraBoldItalic', weight: '800'})};
    ${buildItalic({fileName: 'OpenSans-BoldItalic', weight: '700'})};
    ${buildItalic({fileName: 'OpenSans-SemiBoldItalic', weight: '600'})};
    ${buildItalic({fileName: 'OpenSans-RegularItalic', weight: '400'})};
  `;
};

export default css`
  ${barlow()}
  ${openSans()}
`;
