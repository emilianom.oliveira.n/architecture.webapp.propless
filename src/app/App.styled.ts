
import styled, {css} from 'styled-components';
import {mqTablet} from '~/styles';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

export const PageWrapper = styled.div`
  min-height: calc(100vh - 70px);
  padding: 30px 20px;

  ${mqTablet(css`
    padding: 70px 50px;
  `)}
`;
