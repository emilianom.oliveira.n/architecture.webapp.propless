
import {useHookstate} from '@hookstate/core';
import {
 Action, Location, createBrowserHistory,
} from 'history';
import {
 Redirect, Route, Router, Switch,
} from 'react-router-dom';

import {Routes} from '~/consts';
import {
 AboutPage, HomePage, LoginPage,
} from '~/pages';
import {Header, Loading} from '~/partials';
import {AppStore, UserDataStore} from '~/stores';
import {GlobalStyles, ThemeProvider} from '~/theme';
import {PageWrapper, Wrapper} from './App.styled';

const handleNavigation = (location: Location, action: Action) => {
  AppStore.handleNavigationUpdate({
    action,
    location,
  });
};

const history = createBrowserHistory();
history.listen(handleNavigation);
handleNavigation(history.location, history.action);

const App = () => {

  const {sessionStatus} = useHookstate(UserDataStore.state);

  return (
    <Wrapper className="App">
      <GlobalStyles />
      <ThemeProvider>
        <Router history={history}>
          <Header />
          <PageWrapper>
            <Switch>
              <Route path={Routes.about} exact component={AboutPage} />

              {/* Active session routing bellow. */}
              {sessionStatus.get() && (
                <Route path={Routes.home} exact component={HomePage} />
              )}
              {sessionStatus.get() && (
                <Redirect to={Routes.home} />
              )}

              {/* Inactive session routing bellow. */}
              {!sessionStatus.get() && (
                <Route path={Routes.login} exact component={LoginPage} />
              )}

              {!sessionStatus.get() && (
                <Redirect to={Routes.login} />
              )}

              {/* { UserDataStore.sessionStatus ? (
                <>
                  <Route path={ROUTES.ABOUT} exact component={AboutPage} />
                  <Route path={ROUTES.HOME} exact component={HomePage} />
                  <Redirect to={ROUTES.HOME} />
                </>
              ) : (
                <>
                  <Route path={ROUTES.ABOUT} exact component={AboutPage} />
                  <Route path={ROUTES.LOGIN} exact component={LoginPage} />
                  <Redirect to={ROUTES.LOGIN} />
                </>
              ) } */}
            </Switch>
          </PageWrapper>
        </Router>
        <Loading />
      </ThemeProvider>
    </Wrapper>
  );
};

export default App;
