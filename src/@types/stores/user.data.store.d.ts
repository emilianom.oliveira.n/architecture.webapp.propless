
declare namespace UserDataStore {
  interface State {
    ip: string;
    email: string;
    name: {
      first: string;
      last: string;
    },
    fullName: string;
    location: {
      latitude: number;
      longitude: number;
      state: string;
      city: string;
      country: string;
    };
    sessionStatus: boolean;
  }
}
