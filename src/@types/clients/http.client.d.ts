
declare namespace HTTPClient {
  type Seeder = ExternalModules.Faker.FakerStatic;

  type Mock<TResponse> = {
    builder: (seeder: Seeder) => TResponse;
    status?: ExternalModules.HTTPStatusCodes.StatusCodes;
  };

  type Props = {
    config?: ExternalModules.Axios.AxiosRequestConfig;
    allowMocking?: boolean;
  };

  type GetParams<TResponse> = {
    options?: ExternalModules.Axios.AxiosRequestConfig;
    path: string;
    // url?: string;
    mock?: Mock<TResponse>;
  };

  type PostParams<TResponse> = {
    body: any;
  } & GetParams<TResponse>;
}
