# React - Template Arquitetural

Inicializado com [Create React App](https://github.com/facebook/create-react-app).

## Descrição
Esse projeto foi implementado afim de materializar os conceitos presentes na Arquitetura Propless.

## Overview Arquitetural
![Scheme](public/readme/architectural-diagram.png "Diagrama da Arquitetura")
Diagrama geral da arquitetura.

## Como Rodar o Projeto
Sem mistérios por aqui: `yarn install && yarn start`

## Links Úteis
- [Diagrama Completo](https://miro.com/app/board/o9J_ku7q5Vg=/)
- [Artigo](https://medium.com/@emilianooliveira/the-propless-architecture-6eb11f234de2)
